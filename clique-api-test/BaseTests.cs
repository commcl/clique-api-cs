﻿namespace CliqueApiTest
{
    /// <summary>
    /// Class with common info for all tests.
    /// </summary>
    public class BaseTests
    {
        /// <summary>
        /// API base URL.
        /// </summary>
        protected const string ApiUrl = "https://clique-core-test.caw.me/api/v2";

        /// <summary>
        /// Test authorization key.
        /// </summary>
        protected const string ApiKey = "Bearer 809f375d-1e25-49f2-99c3-a774f3a31e37";
    }
}
