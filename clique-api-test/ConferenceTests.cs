﻿namespace CliqueApiTest
{
    using System;
    using System.Linq;

    using CliqueApi;
    using CliqueApi.Dto.Common;
    using CliqueApi.Dto.Conference;

    using NUnit.Framework;

    /// <summary>
    /// Test class for conferences.
    /// </summary>
    [TestFixture]
    [Parallelizable(ParallelScope.Self)]
    public class ConferenceTests : BaseTests
    {
        /// <summary>
        /// Sample test for url of sign out.
        /// </summary>
        private const string SourceSignOutUrl = "http://karkacctroy.ru/gfyagalick/imgs144916.jpg";

        /// <summary>
        /// Method for creating new conference and test of creation.
        /// </summary>
        /// <param name="apiObject"> API object. </param>
        /// <param name="creatorUuid"> Uuid of creator. </param>
        /// <returns> Newly created conference. </returns>
        public static Conference TestConferenceCreation(ICliqueApi apiObject, string creatorUuid = null)
        {
            var sourceConference = new Conference();
            sourceConference.StartTime = DateTime.UtcNow;
            sourceConference.SignoutUrl = SourceSignOutUrl;
            sourceConference.CreatorUuid = creatorUuid;

            var createdConference = apiObject.ConferenceCreate(sourceConference).Result.Conference;

            Assert.IsNotEmpty(createdConference.Id, "Unable to get conference ID after conference creation.");
            Assert.AreNotEqual(createdConference.Id, sourceConference.Id, "ID of new conference and ID of created conference are equal.");
            if (createdConference.StartTime.HasValue)
            {
                var deltaTime = createdConference.StartTime - sourceConference.StartTime; // Because the sended DateTime more accurate than received
                Assert.IsTrue(Math.Abs(deltaTime.Value.TotalMilliseconds) < 1, "The start_time of the new object is different from the start_time of the added conference.");
            }
            Assert.AreEqual(sourceConference.SignoutUrl, createdConference.SignoutUrl, "The signout_url of the new object is different from the signout_url of the added conference.");
            Assert.IsFalse(ReferenceEquals(sourceConference, createdConference));
            return createdConference;
        }

        /// <summary>
        /// Method for test of CREATE, READ and UPDATE of conferences.
        /// </summary>
        [Test]
        public void ConferenceCrudTestMethod()
        {
            const string UpdatedSignOutUrl = "http://bbsimg.ngfiles.com/1/6333000/ngbbs4318474693c3b.jpg";

            var apiObject = ApiHelper.CreateApiObject(ApiUrl, ApiKey);

            var createdConference = TestConferenceCreation(apiObject);

            var gettedConference = apiObject.ConferenceGetById(createdConference.Id).Result.Conference;

            Assert.AreEqual(createdConference, gettedConference, "Created conference are not equal to the getted conference.");
            Assert.IsFalse(object.ReferenceEquals(createdConference, gettedConference));

            var conferenceSearcher = new Conference();
            conferenceSearcher.SignoutUrl = SourceSignOutUrl;

            var findedConferences = apiObject.ConferencesSearch(conferenceSearcher).Result.Conferences;

            Assert.IsNotEmpty(findedConferences, "Unable to get conferences search result.");

            var findedConference = findedConferences.FirstOrDefault(u => u.Id == gettedConference.Id);

            Assert.IsNotNull(findedConference, "Unable to find new added conference.");

            findedConference.SignoutUrl = UpdatedSignOutUrl;

            var updatedConference = apiObject.ConferenceUpdate(findedConference.Id, findedConference).Result.Conference;

            Assert.AreEqual(findedConference.SignoutUrl, updatedConference.SignoutUrl, "Finded conference are not equal to the getted conference.");
        }

        /// <summary>
        /// Method for testing getting and updating user settings
        /// </summary>
        [Test]
        public void TestConferenceSettings()
        {
            var apiObject = ApiHelper.CreateApiObject(ApiUrl, ApiKey);

            var createdConference = TestConferenceCreation(apiObject);

            var conferenceSettings = apiObject.ConferenceGetSettings(createdConference.Id).Result.Settings;

            var updatingSettings = new Settings();

            updatingSettings.Id = conferenceSettings.Id;
            updatingSettings.IsWaitForModerator = !conferenceSettings.IsWaitForModerator;
            updatingSettings.IsE1JoinsMuted = !conferenceSettings.IsE1JoinsMuted;
            updatingSettings.IsJoinInWatchMode = !conferenceSettings.IsJoinInWatchMode;
            updatingSettings.IsMusicOnHold = !conferenceSettings.IsMusicOnHold;
            updatingSettings.IsAllowPstnCalls = !conferenceSettings.IsAllowPstnCalls;

            var updatedResponse = apiObject.ConferenceUpdateSettings(createdConference.Id, updatingSettings).Result;

            Assert.IsTrue(updatedResponse.IsOk, "Fail in updating conference settings.");

            var updatedSettings = apiObject.ConferenceGetSettings(createdConference.Id).Result.Settings;

            Assert.True(updatingSettings.Equals(updatedSettings), "Finded conference settings are not equal to the getted settings.");
        }

        /// <summary>
        /// Method for testing getting user list of conference.
        /// </summary>
        [Test]
        public void TestConferenceUserList()
        {
            // Conference ID getted manually from DB
            const string ConferenceId = "14836c5c-7f68-41f9-8bca-e185faad105f";

            var apiObject = ApiHelper.CreateApiObject(ApiUrl, ApiKey);

            //// TODO: надо создать как-нибудь конференцию с пользователем

            var conferenceResponse = apiObject.ConferenceUserList(ConferenceId).Result;

            Assert.IsTrue(conferenceResponse.IsOk, "Fail in getting conference user list");
        }

        /// <summary>
        /// Method for testing start conference.
        /// </summary>
        [Test]
        public void TestConferenceStart()
        {
            var apiObject = ApiHelper.CreateApiObject(ApiUrl, ApiKey);

            var createdConference = TestConferenceCreation(apiObject);

            var startedConference = apiObject.ConferenceStart(createdConference.Id).Result.Conference;

            Assert.IsNotEmpty(startedConference.NodeDomain, "Unable to start conference.");

            createdConference.NodeDomain = startedConference.NodeDomain;
            Assert.AreEqual(createdConference, startedConference, "Created and started conference are not equal.");
        }
    }
}
