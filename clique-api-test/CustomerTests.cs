﻿namespace CliqueApiTest
{
    using System.Linq;

    using CliqueApi;
    using CliqueApi.Dto.Customer;

    using NUnit.Framework;

    /// <summary>
    /// Test class for customers.
    /// </summary>
    [TestFixture]
    [Parallelizable(ParallelScope.Self)]
    public class CustomerTests : BaseTests
    {
        /// <summary>
        /// Method for test of CREATE, READ and UPDATE of customers.
        /// </summary>
        [Test]
        public void CustomerCrudTestMethod()
        {
            var apiObject = ApiHelper.CreateApiObject(ApiUrl, ApiKey);

            const string SourceExternalId = "yet_anoter_external_id_for_test";

            var createdCustomer = apiObject.CustomerCreate(new CustomerCreationQuery(SourceExternalId)).Result.Customer;

            Assert.IsTrue(createdCustomer.Id > 0, "Unable to get customer ID after customer creation.");
            Assert.AreEqual(createdCustomer.ExternalId, SourceExternalId, "The external_id of the new object is different from the external_id of the added customer.");

            var gettedCustomer = apiObject.CustomerGetById(createdCustomer.Id).Result.Customer;

            Assert.AreEqual(createdCustomer, gettedCustomer, "Created customer are not equal to the getted customer.");
            Assert.IsFalse(object.ReferenceEquals(createdCustomer, gettedCustomer));

            var customerSearcher = new Customer();
            customerSearcher.Id = gettedCustomer.Id;
            customerSearcher.Token = gettedCustomer.Token;

            var findedCustomers = apiObject.CustomerSearch(customerSearcher).Result.Customers;

            Assert.IsNotEmpty(findedCustomers, "Unable to get customers search result.");

            var findedCustomer = findedCustomers.FirstOrDefault(u => u.Id == gettedCustomer.Id);

            Assert.IsNotNull(findedCustomer, "Unable to find new added customer.");

            findedCustomer.DisplayName = "Update Customer for Update 1";

            var updatedCustomer = apiObject.CustomerUpdate(findedCustomer.Id, findedCustomer).Result.Customer;

            Assert.AreEqual(findedCustomer.DisplayName, updatedCustomer.DisplayName, "Finded customer are not equal to the getted customer.");
        }
    }
}
