﻿namespace CliqueApiTest
{
    using System;
    using System.Collections.Generic;
    using System.Net;

    using CliqueApi;
    using CliqueApi.Dto.Invite;

    using NUnit.Framework;

    /// <summary>
    /// Test class for testing invitation and nodes methods of API.
    /// </summary>
    [TestFixture]
    [Parallelizable(ParallelScope.Self)]
    public class InvitationsTest : BaseTests
    {
        /// <summary>
        /// Method for test of CREATE, READ and UPDATE of users.
        /// </summary>
        [Test]
        public void TemplateCrudTestMethod()
        {
            var apiObject = ApiHelper.CreateApiObject(ApiUrl, ApiKey);

            var templateName = Guid.NewGuid().ToString().Substring(0, 27);
            var templateBody = "PREVED {username}! KAGDILA?";

            var createdTemplate = TestTemplateCreation(apiObject, templateName, templateBody, ContactType.email);

            var gettedTemplate = apiObject.InvitationTemplateGetById(templateName).Result.Template;

            Assert.AreEqual(createdTemplate, gettedTemplate, "Created user are not equal to the getted user.");
            Assert.IsFalse(object.ReferenceEquals(createdTemplate, gettedTemplate));

            var updatingTemplate = new Template();
            updatingTemplate.Name = templateName;
            updatingTemplate.TemplateBody = "Hi, {username}! How are you?";

            var updatedTemplate = apiObject.InvitationTemplateUpdate(templateName, updatingTemplate).Result.Template;

            Assert.AreEqual(updatedTemplate.Name, templateName);
            Assert.AreEqual(updatedTemplate.TemplateBody, updatingTemplate.TemplateBody);
            Assert.AreEqual(updatedTemplate.Type, createdTemplate.Type);

            TestDeleteTemplate(apiObject, updatedTemplate);
        }

        /// <summary>
        /// Method for test sending invitation and get initial data.
        /// </summary>
        [Test]
        public void InvitationTest()
        {
            var apiObject = ApiHelper.CreateApiObject(ApiUrl, ApiKey);

            var createdUser = UserTests.TestUserCreation(apiObject, Guid.NewGuid().ToString());
            var createdConference = ConferenceTests.TestConferenceCreation(apiObject, createdUser.Uuid);

            var messages = new InviteMessages();

            var templateSmsName = Guid.NewGuid().ToString().Substring(0, 27);
            var templateSmsBody = "PREVED {username}! KAGDILA?";

            var createdSmsTemplate = TestTemplateCreation(apiObject, templateSmsName, templateSmsBody, ContactType.sms);

            messages.Sms = new TextMessage();
            var smsContact = new ContactRecord();
            smsContact.Contact = "+79234343038";
            smsContact.Name = "Alex1";
            messages.Sms.Contacts = new[] { smsContact };
            messages.Sms.TemplateName = templateSmsName;
            messages.Sms.TemplateParams = new Dictionary<string, string> { { "username", "Kim Il Sung" } };

            var templateEmailName = Guid.NewGuid().ToString().Substring(0, 27);
            var templateEmailBody = "Hi, {username}! How are you?";

            var createdEmailTemplate = TestTemplateCreation(apiObject, templateEmailName, templateEmailBody, ContactType.email);

            messages.Email = new TextMessage();
            var emailContact = new ContactRecord();
            emailContact.Contact = "phoenix42@mail.ru";
            emailContact.Name = "Alex2";
            messages.Email.Contacts = new[] { emailContact };
            messages.Email.TemplateName = templateEmailName;
            messages.Email.TemplateParams = new Dictionary<string, string> { { "username", "Kim Jong Un" } };

            var invitationFailures = apiObject.SendInvitesForConference(createdConference.Id, messages).Result.Failures;

            Assert.IsEmpty(invitationFailures, "Errors in sending invitation messages");

            TestDeleteTemplate(apiObject, createdSmsTemplate);
            TestDeleteTemplate(apiObject, createdEmailTemplate);
        }

        /// <summary>
        /// Method for creating new user and test of creation.
        /// </summary>
        /// <param name="apiObject"> API object. </param>
        /// <param name="uniqueName"> The unique Name. </param>
        /// <param name="template"> The template. </param>
        /// <param name="type"> Contact type. </param>
        /// <returns>
        /// Newly created user. 
        /// </returns>
        private static Template TestTemplateCreation(ICliqueApi apiObject, string uniqueName, string template, ContactType type)
        { 
            var sourceTemplate = new Template();
            sourceTemplate.Name = uniqueName;
            sourceTemplate.TemplateBody = template;
            sourceTemplate.Type = type;

            var createdTemplate = apiObject.InvitationTemplateCreate(sourceTemplate).Result.Template;

            Assert.IsTrue(createdTemplate.CustomerId > 0, "Wrong customer_id of template after creation.");
            Assert.AreEqual(createdTemplate.Name, sourceTemplate.Name, "Name of creating template is not equal a template's name after creation");
            Assert.AreEqual(createdTemplate.TemplateBody, sourceTemplate.TemplateBody, "Template of creating template is not equal a template's template (yea!) after creation");
            Assert.AreEqual(createdTemplate.Type, sourceTemplate.Type, "Type of creating template is not equal a template's type after creation");

            return createdTemplate;
        }

        /// <summary>
        /// The test delete template.
        /// </summary>
        /// <param name="api">
        /// The api.
        /// </param>
        /// <param name="template">
        /// The template.
        /// </param>
        private static void TestDeleteTemplate(ICliqueApi api, Template template)
        {
            api.InvitationTemplateDelete(template.Name).Wait();

            try
            {
                api.InvitationTemplateGetById(template.Name).Wait();
                Assert.Fail("User not deleted");
            }
            catch (AggregateException ex)
            {
                var error = ApiHelper.InterpretApiException(ex);

                Assert.NotNull(error, "Unable to get error info from exception.");
                Assert.AreEqual(error.StatusCode, HttpStatusCode.NotFound, "Invalid status of response.");
            }
        }
    }
}
