﻿namespace CliqueApiTest
{
    using System.Linq;

    using CliqueApi;

    using NUnit.Framework;

    /// <summary>
    /// Test class for testing invitation and nodes methods of API.
    /// </summary>
    [TestFixture]
    [Parallelizable(ParallelScope.Self)]
    public class NodesTest : BaseTests
    {
        /// <summary>
        /// The nodes test.
        /// </summary>
        [Test]
        public void NodesTestMethod()
        {
            var apiObject = ApiHelper.CreateApiObject(ApiUrl, ApiKey);

            var nodes = apiObject.NodesGetActive().Result.Nodes;

            if (nodes.Count <= 0)
            {
                return;
            }

            var firstNode = nodes.First();
            Assert.NotNull(firstNode, "The first node is null");

            Assert.IsNotEmpty(firstNode.DomainName, "Empty domain name of node");
            Assert.IsNotEmpty(firstNode.Port, "Empty port of node");
        }
    }
}
