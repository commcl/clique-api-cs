﻿namespace CliqueApiTest
{
    using System;
    using System.Linq;

    using CliqueApi;
    using CliqueApi.Dto.User;

    using NUnit.Framework;

    /// <summary>
    /// Test class for users.
    /// </summary>
    [TestFixture]
    [Parallelizable(ParallelScope.Self)]
    public class UserTests : BaseTests
    {
        /// <summary>
        /// Method for creating new user and test of creation.
        /// </summary>
        /// <param name="apiObject"> API object. </param>
        /// <param name="externalId"> Display name of user. </param>
        /// <returns> Newly created user. </returns>
        public static User TestUserCreation(ICliqueApi apiObject, string externalId)
        {
            var sourceUser = new User { ExternalId = externalId };

            var createdUser = apiObject.UserCreate(sourceUser).Result.User;

            Assert.IsNotEmpty(createdUser.Uuid, "Unable to get user UUID after user creation.");
            Assert.AreNotEqual(createdUser.Uuid, sourceUser.Uuid, "UUID of new user and ID of created user are equal.");
            Assert.AreEqual(sourceUser.ExternalId, createdUser.ExternalId, "The external_id of the new object is different from the external_id of the added user.");
            Assert.IsFalse(object.ReferenceEquals(sourceUser, createdUser));
            return createdUser;
        }

        /// <summary>
        /// Method for test of CREATE, READ and UPDATE of users.
        /// </summary>
        [Test]
        public void UserCrudTestMethod()
        {
            var apiObject = ApiHelper.CreateApiObject(ApiUrl, ApiKey);
            
            var createdUser = TestUserCreation(apiObject, Guid.NewGuid().ToString());

            var gettedUser = apiObject.UserGetByUuid(createdUser.Uuid).Result.User;

            Assert.AreEqual(createdUser, gettedUser, "Created user are not equal to the getted user.");
            Assert.IsFalse(object.ReferenceEquals(createdUser, gettedUser));

            var userSearcher = new User();
            userSearcher.CustomerId = gettedUser.CustomerId;
            userSearcher.IsEnabled = gettedUser.IsEnabled;

            var findedUsers = apiObject.UsersSearch(userSearcher).Result.Users;

            Assert.IsNotEmpty(findedUsers, "Unable to get users search result.");

            var findedUser = findedUsers.FirstOrDefault(u => u.Uuid == gettedUser.Uuid);

            Assert.IsNotNull(findedUser, "Unable to find new added user.");

            findedUser.ExternalId = Guid.NewGuid().ToString();

            var updatedUser = apiObject.UserUpdate(findedUser.Uuid, findedUser).Result.User;

            Assert.AreEqual(findedUser.ExternalId, updatedUser.ExternalId, "Finded user are not equal to the getted user.");
        }

        /// <summary>
        /// Method for testing getting user history.
        /// </summary>
        [Test]
        public void TestUserHistory()
        {
            var apiObject = ApiHelper.CreateApiObject(ApiUrl, ApiKey);

            // This UUID taked manually from DB.
            var userSearcher = new User { Uuid = "3b475b85-1c73-4373-8ac2-4b230fcecc26" };

            var findedUser = apiObject.UsersSearch(userSearcher).Result.Users.SingleOrDefault();

            Assert.IsNotNull(findedUser, "Unable to find user by UUID for getting history.");

            var history = apiObject.UserGetHistory(findedUser.Uuid, 10, 0).Result;

            if (!history.IsOk)
            {
                Assert.Fail("Error in getting history for user");
            }
        }

        /// <summary>
        /// Method for testing change user token.
        /// </summary>
        [Test]
        public void TestUserToken()
        {
            var apiObject = ApiHelper.CreateApiObject(ApiUrl, ApiKey);

            var createdUser = TestUserCreation(apiObject, Guid.NewGuid().ToString());

            var newToken = apiObject.UserGetToken(createdUser.Uuid).Result.Token;

            Assert.IsNotEmpty(newToken, "Unable to get new user token");
            Assert.AreNotEqual(createdUser.Token, newToken);
        }
    }
}
