﻿namespace CliqueApi
{
    using System;
    using System.Net;

    using CliqueApi.Dto.Error;

    using Newtonsoft.Json.Linq;

    using RestEase;

    /// <summary>
    /// Helper methods for Cluque REST API.
    /// </summary>
    public static class ApiHelper
    {
        /// <summary>
        /// Create object of API interface.
        /// </summary>
        /// <param name="apiUrl"> Base url of Cluque REST API. </param>
        /// <param name="apiKey"> Authorization key. </param>
        /// <returns>
        /// Interface object <see cref="ICliqueApi"/>.
        /// </returns>
        public static ICliqueApi CreateApiObject(string apiUrl, string apiKey)
        {
            ServicePointManager.Expect100Continue = true;
            ServicePointManager.SecurityProtocol = (SecurityProtocolType)3072;

            var restClient = new RestClient(apiUrl) { RequestQueryParamSerializer = new SearchQuerySerializer() };

            var apiObject = restClient.For<ICliqueApi>();
            apiObject.ApiKey = apiKey;

            return apiObject;
        }

        /// <summary>
        /// Interpretate <see cref="System.Threading.Tasks.Task"/> exception, was thrown by async API methods.
        /// </summary>
        /// <param name="ex"> Base exception. </param>
        /// <returns>
        /// Information about error <see cref="CliqueApi.Dto.Error"/>.
        /// </returns>
        public static Error InterpretApiException(AggregateException ex)
        {
            var apiException = ex.InnerException as ApiException;
            if (apiException == null)
            {
                return null;
            }

            return InterpretApiException(apiException);
        }

        /// <summary>
        /// Interpretate catched API exception <see cref="ApiException"/>.
        /// </summary>
        /// <param name="ex"> Api exception object. </param>
        /// <returns>
        /// Information about error <see cref="CliqueApi.Dto.Error"/>.
        /// </returns>
        public static Error InterpretApiException(ApiException ex)
        {
            if (string.IsNullOrWhiteSpace(ex.Content))
            {
                return null;
            }

            try
            {
                var jsonObject = JObject.Parse(ex.Content);

                var errorToken = jsonObject["error"];

                var error = errorToken.ToObject<Error>();

                error.StatusCode = ex.StatusCode;

                return error;
            }
            catch
            {
                return null;
            }
        }
    }
}
