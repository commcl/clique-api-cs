namespace CliqueApi.Dto.Conference
{
    using System;

    using CliqueApi.Dto.Common;

    using Newtonsoft.Json;

    public class Conference : IEquatable<Conference>
    {
        [JsonProperty("id", DefaultValueHandling = DefaultValueHandling.Ignore)]
        public string Id { get; set; }

        [JsonProperty("start_time", DefaultValueHandling = DefaultValueHandling.Ignore)]
        public DateTime? StartTime { get; set; }

        [JsonProperty("signout_url", DefaultValueHandling = DefaultValueHandling.Ignore)]
        public string SignoutUrl { get; set; }

        [JsonProperty("node_domain", DefaultValueHandling = DefaultValueHandling.Ignore)]
        public string NodeDomain { get; set; }

        [JsonProperty("node_autoassign", DefaultValueHandling = DefaultValueHandling.Ignore)]
        public string NodeAutoassign { get; set; }

        [JsonProperty("customer_id", DefaultValueHandling = DefaultValueHandling.Ignore)]
        public int CustomerId { get; set; }

        [JsonProperty("allow_anonymous", DefaultValueHandling = DefaultValueHandling.Ignore)]
        public bool AllowAnonymous { get; set; }

        [JsonProperty("locked", DefaultValueHandling = DefaultValueHandling.Ignore)]
        public bool Locked { get; set; }

        [JsonProperty("creator_uuid", DefaultValueHandling = DefaultValueHandling.Ignore)]
        public string CreatorUuid { get; set; }

        [JsonProperty("Settings", DefaultValueHandling = DefaultValueHandling.Ignore)]
        public Settings Settings { get; set; }

        public bool Equals(Conference other)
        {
            if (ReferenceEquals(null, other))
            {
                return false;
            }

            if (ReferenceEquals(this, other))
            {
                return true;
            }

            return string.Equals(this.Id, other.Id)
                && this.StartTime.Equals(other.StartTime)
                && string.Equals(this.SignoutUrl, other.SignoutUrl)
                && string.Equals(this.NodeDomain, other.NodeDomain)
                && string.Equals(this.NodeAutoassign, other.NodeAutoassign)
                && this.CustomerId == other.CustomerId
                && this.AllowAnonymous == other.AllowAnonymous
                && this.Locked == other.Locked
                && string.Equals(this.CreatorUuid, other.CreatorUuid)
                && Equals(this.Settings, other.Settings);
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj))
            {
                return false;
            }

            if (ReferenceEquals(this, obj))
            {
                return true;
            }

            if (obj.GetType() != this.GetType())
            {
                return false;
            }

            return Equals((Conference)obj);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                var hashCode = (this.Id != null ? this.Id.GetHashCode() : 0);
                hashCode = (hashCode * 397) ^ this.StartTime.GetHashCode();
                hashCode = (hashCode * 397) ^ (this.SignoutUrl != null ? this.SignoutUrl.GetHashCode() : 0);
                hashCode = (hashCode * 397) ^ (this.NodeDomain != null ? this.NodeDomain.GetHashCode() : 0);
                hashCode = (hashCode * 397) ^ (this.NodeAutoassign != null ? this.NodeAutoassign.GetHashCode() : 0);
                hashCode = (hashCode * 397) ^ this.CustomerId;
                hashCode = (hashCode * 397) ^ this.AllowAnonymous.GetHashCode();
                hashCode = (hashCode * 397) ^ this.Locked.GetHashCode();
                hashCode = (hashCode * 397) ^ (this.CreatorUuid != null ? this.CreatorUuid.GetHashCode() : 0);
                hashCode = (hashCode * 397) ^ (this.Settings != null ? this.Settings.GetHashCode() : 0);
                return hashCode;
            }
        }
    }
}