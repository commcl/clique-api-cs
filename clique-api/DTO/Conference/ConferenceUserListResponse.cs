namespace CliqueApi.Dto.Conference
{
    using System.Collections.Generic;

    using Newtonsoft.Json;

    public class ConferenceUserListResponse : BaseResponse
    {
        [JsonProperty("result")]
        public IDictionary<string, User.User> UserList { get; set; }
    }
}