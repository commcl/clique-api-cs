namespace CliqueApi.Dto.Error
{
    using Newtonsoft.Json;

    public class ErrorDescriptionInfo
    {
        [JsonProperty("param")]
        public string ParameterName { get; set; }

        [JsonProperty("msg")]
        public string Message { get; set; }

        [JsonProperty("value")]
        public string Value { get; set; }
    }
}