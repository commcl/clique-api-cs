namespace CliqueApi.Dto.User
{
    using System;

    using CliqueApi.Dto.Common;

    using Newtonsoft.Json;

    public class User : IEquatable<User>
    {
        [JsonProperty("uuid", DefaultValueHandling = DefaultValueHandling.Ignore)]
        public string Uuid { get; set; }

        [JsonProperty("external_id", DefaultValueHandling = DefaultValueHandling.Ignore)]
        public string ExternalId { get; set; }

        [JsonProperty("customer_id", DefaultValueHandling = DefaultValueHandling.Ignore)]
        public int CustomerId { get; set; }

        [JsonProperty("sip_id", DefaultValueHandling = DefaultValueHandling.Ignore)]
        public string SipId { get; set; }

        [JsonProperty("sip_pwd", DefaultValueHandling = DefaultValueHandling.Ignore)]
        public string SipPwd { get; set; }

        [JsonProperty("enabled", DefaultValueHandling = DefaultValueHandling.Ignore)]
        public bool IsEnabled { get; set; }

        [JsonProperty("token", DefaultValueHandling = DefaultValueHandling.Ignore)]
        public string Token { get; set; }

        [JsonProperty("settings", DefaultValueHandling = DefaultValueHandling.Ignore)]
        public Settings Settings { get; set; }

        public bool Equals(User other)
        {
            if (ReferenceEquals(null, other))
            {
                return false;
            }

            if (ReferenceEquals(this, other))
            {
                return true;
            }

            return string.Equals(this.Uuid, other.Uuid)
                && string.Equals(this.ExternalId, other.ExternalId)
                && this.CustomerId == other.CustomerId
                && string.Equals(this.SipId, other.SipId)
                && string.Equals(this.SipPwd, other.SipPwd)
                && this.IsEnabled == other.IsEnabled;
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj))
            {
                return false;
            }

            if (ReferenceEquals(this, obj))
            {
                return true;
            }

            if (obj.GetType() != this.GetType())
            {
                return false;
            }

            return Equals((User)obj);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                var hashCode = (this.Uuid != null ? this.Uuid.GetHashCode() : 0);
                hashCode = (hashCode * 397) ^ (this.ExternalId != null ? this.ExternalId.GetHashCode() : 0);
                hashCode = (hashCode * 397) ^ this.CustomerId;
                hashCode = (hashCode * 397) ^ (this.SipId != null ? this.SipId.GetHashCode() : 0);
                hashCode = (hashCode * 397) ^ (this.SipPwd != null ? this.SipPwd.GetHashCode() : 0);
                hashCode = (hashCode * 397) ^ this.IsEnabled.GetHashCode();
                return hashCode;
            }
        }
    }
}