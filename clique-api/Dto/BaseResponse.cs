﻿namespace CliqueApi.Dto
{
    using Newtonsoft.Json;

    /// <summary>
    /// Base class of JSON responses.
    /// </summary>
    public class BaseResponse
    {
        /// <summary>
        /// Response successfull status.
        /// </summary>
        [JsonProperty("ok")]
        public bool IsOk { get; set; }
    }
}
