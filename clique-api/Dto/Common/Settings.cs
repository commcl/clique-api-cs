﻿namespace CliqueApi.Dto.Common
{
    using System;

    using Newtonsoft.Json;

    public class Settings : IEquatable<Settings>
    {
        [JsonProperty("id", DefaultValueHandling = DefaultValueHandling.Ignore)]
        public int Id { get; set; }

        [JsonProperty("wait_for_moderator")]
        public bool? IsWaitForModerator { get; set; }

        [JsonProperty("allow_pstn_calls")]
        public bool IsAllowPstnCalls { get; set; }

        [JsonProperty("e1_joins_muted")]
        public bool IsE1JoinsMuted { get; set; }

        [JsonProperty("join_in_watch_mode")]
        public bool IsJoinInWatchMode { get; set; }

        [JsonProperty("music_on_hold")]
        public bool IsMusicOnHold { get; set; }

        public bool Equals(Settings other)
        {
            if (ReferenceEquals(null, other))
            {
                return false;
            }

            if (ReferenceEquals(this, other))
            {
                return true;
            }

            return this.Id == other.Id 
                && this.IsWaitForModerator == other.IsWaitForModerator 
                && this.IsAllowPstnCalls == other.IsAllowPstnCalls 
                && this.IsE1JoinsMuted == other.IsE1JoinsMuted 
                && this.IsJoinInWatchMode == other.IsJoinInWatchMode 
                && this.IsMusicOnHold == other.IsMusicOnHold;
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj))
            {
                return false;
            }

            if (ReferenceEquals(this, obj))
            {
                return true;
            }

            if (obj.GetType() != this.GetType())
            {
                return false;
            }

            return this.Equals((Settings)obj);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                var hashCode = this.Id;
                hashCode = (hashCode * 397) ^ this.IsWaitForModerator.GetHashCode();
                hashCode = (hashCode * 397) ^ this.IsAllowPstnCalls.GetHashCode();
                hashCode = (hashCode * 397) ^ this.IsE1JoinsMuted.GetHashCode();
                hashCode = (hashCode * 397) ^ this.IsJoinInWatchMode.GetHashCode();
                hashCode = (hashCode * 397) ^ this.IsMusicOnHold.GetHashCode();
                return hashCode;
            }
        }
    }
}
