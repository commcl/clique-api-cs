﻿namespace CliqueApi.Dto.Common
{
    using Newtonsoft.Json;

    public class SettingsResponse : BaseResponse
    {
        [JsonProperty("settings")]
        public Settings Settings { get; set; }
    }
}
