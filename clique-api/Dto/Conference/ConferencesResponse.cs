﻿namespace CliqueApi.Dto.Conference
{
    using System.Collections.Generic;

    using Newtonsoft.Json;

    public class ConferencesResponse : BaseResponse
    {
        [JsonProperty("result")]
        public IList<Conference> Conferences { get; set; }
    }
}
