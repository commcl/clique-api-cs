namespace CliqueApi.Dto.Conference
{
    using Newtonsoft.Json;

    public class SingleConferenceResponse : BaseResponse
    {
        [JsonProperty("conference")]
        public Conference Conference { get; set; }
    }
}