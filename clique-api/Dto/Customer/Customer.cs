﻿namespace CliqueApi.Dto.Customer
{
    using System;

    using CliqueApi.Dto.Common;

    using Newtonsoft.Json;

    public class Customer : IEquatable<Customer>
    {
        [JsonProperty("id", DefaultValueHandling = DefaultValueHandling.Ignore)]
        public int Id { get; set; }

        [JsonProperty("external_id", DefaultValueHandling = DefaultValueHandling.Ignore)]
        public string ExternalId { get; set; }

        [JsonProperty("display_name", DefaultValueHandling = DefaultValueHandling.Ignore)]
        public string DisplayName { get; set; }

        [JsonProperty("token", DefaultValueHandling = DefaultValueHandling.Ignore)]
        public string Token { get; set; }

        [JsonProperty("superuser", DefaultValueHandling = DefaultValueHandling.Ignore)]
        public bool IsSuperuser { get; set; }

        [JsonProperty("SettingsTemplate", DefaultValueHandling = DefaultValueHandling.Ignore)]
        public Settings Settings { get; set; }

        public bool Equals(Customer other)
        {
            if (ReferenceEquals(null, other))
            {
                return false;
            }

            if (ReferenceEquals(this, other))
            {
                return true;
            }

            return this.Id == other.Id
                && string.Equals(this.ExternalId, other.ExternalId)
                && string.Equals(this.DisplayName, other.DisplayName)
                && string.Equals(this.Token, other.Token)
                && this.IsSuperuser == other.IsSuperuser
                && Equals(this.Settings, other.Settings);
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj))
            {
                return false;
            }

            if (ReferenceEquals(this, obj))
            {
                return true;
            }

            if (obj.GetType() != this.GetType())
            {
                return false;
            }

            return Equals((Customer)obj);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                var hashCode = this.Id;
                hashCode = (hashCode * 397) ^ (this.ExternalId != null ? this.ExternalId.GetHashCode() : 0);
                hashCode = (hashCode * 397) ^ (this.DisplayName != null ? this.DisplayName.GetHashCode() : 0);
                hashCode = (hashCode * 397) ^ (this.Token != null ? this.Token.GetHashCode() : 0);
                hashCode = (hashCode * 397) ^ this.IsSuperuser.GetHashCode();
                hashCode = (hashCode * 397) ^ (this.Settings != null ? this.Settings.GetHashCode() : 0);
                return hashCode;
            }
        }
    }
}
