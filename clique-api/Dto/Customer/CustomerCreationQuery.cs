﻿namespace CliqueApi.Dto.Customer
{
    using Newtonsoft.Json;

    /// <summary>
    /// Class for creation customer method of API.
    /// </summary>
    public class CustomerCreationQuery
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="CustomerCreationQuery"/> class.
        /// </summary>
        /// <param name="externalId">
        /// The external id.
        /// </param>
        public CustomerCreationQuery(string externalId)
        {
            this.ExternalId = externalId;
        }

        /// <summary>
        /// Customer's id from external service, i.e. slack team id.
        /// </summary>
        [JsonProperty("external_id")]
        public string ExternalId { get; set; }
    }
}
