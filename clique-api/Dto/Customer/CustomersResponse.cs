﻿namespace CliqueApi.Dto.Customer
{
    using System.Collections.Generic;

    using Newtonsoft.Json;

    public class CustomersResponse : BaseResponse
    {
        [JsonProperty("result")]
        public IList<Customer> Customers { get; set; }
    }
}
