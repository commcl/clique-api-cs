﻿namespace CliqueApi.Dto.Customer
{
    using Newtonsoft.Json;

    public class SingleCustomerResponse : BaseResponse
    {
        [JsonProperty("customer")]
        public Customer Customer { get; set; }
    }
}
