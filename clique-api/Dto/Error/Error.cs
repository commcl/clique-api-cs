﻿namespace CliqueApi.Dto.Error
{
    using System.Collections.Generic;
    using System.Net;

    using Newtonsoft.Json;

    public class Error
    {
        [JsonProperty("code")]
        public string Code { get; set; }

        [JsonIgnore]
        public HttpStatusCode StatusCode { get; set; }

        [JsonProperty("desc")]
        public List<ErrorDescriptionInfo> Description { get; set; }
    }
}
