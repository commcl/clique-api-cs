﻿namespace CliqueApi.Dto.Invite
{
    using Newtonsoft.Json;

    public class Call
    {
        [JsonProperty("contacts")]
        public ContactRecord[] Contacts { get; set; }
    }
}
