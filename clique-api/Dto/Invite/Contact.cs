﻿namespace CliqueApi.Dto.Invite
{
    using Newtonsoft.Json;

    public class ContactRecord
    {
        [JsonProperty("contact")]
        public string Contact { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }
    }
}
