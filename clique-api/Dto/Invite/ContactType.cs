﻿namespace CliqueApi.Dto.Invite
{
    public enum ContactType
    {
        none = 0,
        sms,
        email
    }
}
