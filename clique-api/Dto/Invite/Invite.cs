﻿namespace CliqueApi.Dto.Invite
{
    using Newtonsoft.Json;
    using Newtonsoft.Json.Converters;

    public class Invite
    {
        [JsonProperty("status")]
        public string Status { get; set; }

        [JsonProperty("type")]
        [JsonConverter(typeof(StringEnumConverter))]
        public ContactType Type { get; set; }

        [JsonProperty("contact")]
        public string Contact { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("attempts")]
        public int Attempts { get; set; }

        [JsonProperty("formatted_contact")]
        public string FormattedContact { get; set; }
    }
}
