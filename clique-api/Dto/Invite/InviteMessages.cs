﻿namespace CliqueApi.Dto.Invite
{
    using Newtonsoft.Json;

    public class InviteMessages
    {
        [JsonProperty("sms")]
        public TextMessage Sms { get; set; }

        [JsonProperty("email")]
        public TextMessage Email { get; set; }

        [JsonProperty("call")]
        public Call Contacts { get; set; }
    }
}
