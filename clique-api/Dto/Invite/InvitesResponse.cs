namespace CliqueApi.Dto.Invite
{
    using System.Collections.Generic;

    using Newtonsoft.Json;

    public class InvitesResponse : BaseResponse
    {
        [JsonProperty("invites")]
        public IDictionary<string, Invite> Invites { get; set; }
    }
}