﻿namespace CliqueApi.Dto.Invite
{
    using System.Collections.Generic;

    using Newtonsoft.Json;

    public class SendingInviteResponse : BaseResponse
    {
        [JsonProperty("failures")]
        public KeyValuePair<string, string>[] Failures { get; set; }
    }
}
