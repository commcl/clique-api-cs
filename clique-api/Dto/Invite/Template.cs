﻿namespace CliqueApi.Dto.Invite
{
    using System;

    using Newtonsoft.Json;
    using Newtonsoft.Json.Converters;

    public class Template : IEquatable<Template>
    {
        [JsonProperty("name", DefaultValueHandling = DefaultValueHandling.Ignore)]
        public string Name { get; set; }

        [JsonProperty("template", DefaultValueHandling = DefaultValueHandling.Ignore)]
        public string TemplateBody { get; set; }

        [JsonProperty("type", DefaultValueHandling = DefaultValueHandling.Ignore)]
        [JsonConverter(typeof(StringEnumConverter))]
        public ContactType Type { get; set; }

        [JsonProperty("customer_id", DefaultValueHandling = DefaultValueHandling.Ignore)]
        public int CustomerId { get; set; }

        public bool Equals(Template other)
        {
            if (ReferenceEquals(null, other))
            {
                return false;
            }

            if (ReferenceEquals(this, other))
            {
                return true;
            }

            return string.Equals(this.Name, other.Name)
                && string.Equals(this.TemplateBody, other.TemplateBody)
                && this.Type == other.Type
                && this.CustomerId == other.CustomerId;
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj))
            {
                return false;
            }

            if (ReferenceEquals(this, obj))
            {
                return true;
            }

            if (obj.GetType() != this.GetType())
            {
                return false;
            }

            return Equals((Template)obj);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                var hashCode = (this.Name != null ? this.Name.GetHashCode() : 0);
                hashCode = (hashCode * 397) ^ (this.TemplateBody != null ? this.TemplateBody.GetHashCode() : 0);
                hashCode = (hashCode * 397) ^ (int)this.Type;
                hashCode = (hashCode * 397) ^ this.CustomerId;
                return hashCode;
            }
        }
    }
}
