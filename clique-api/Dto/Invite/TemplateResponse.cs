﻿namespace CliqueApi.Dto.Invite
{
    using Newtonsoft.Json;

    public class TemplateResponse : BaseResponse
    {
        [JsonProperty("template")]
        public Template Template { get; set; }
    }
}
