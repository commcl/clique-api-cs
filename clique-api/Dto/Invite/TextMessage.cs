﻿namespace CliqueApi.Dto.Invite
{
    using System.Collections.Generic;

    using Newtonsoft.Json;

    public class TextMessage
    {
        [JsonProperty("template_name")]
        public string TemplateName { get; set; }

        [JsonProperty("template_params")]
        public IDictionary<string, string> TemplateParams { get; set; }

        [JsonProperty("contacts")]
        public ContactRecord[] Contacts { get; set; }
    }
}
