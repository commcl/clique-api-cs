﻿namespace CliqueApi.Dto.Nodes
{
    using Newtonsoft.Json;

    public class Node
    {
        [JsonProperty("domain_name")]
        public string DomainName { get; set; }

        [JsonProperty("cpu_load")]
        public float? CpuLoad { get; set; }

        [JsonProperty("descr")]
        public string Description { get; set; }

        [JsonProperty("port")]
        public string Port { get; set; }
    }
}
