﻿namespace CliqueApi.Dto.Nodes
{
    using System.Collections.Generic;

    using Newtonsoft.Json;

    public class NodesResponse : BaseResponse
    {
        [JsonProperty("result")]
        public List<Node> Nodes { get; set; }
    }
}
