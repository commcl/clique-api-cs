﻿namespace CliqueApi.Dto.User
{
    using Newtonsoft.Json;

    public class History
    {
        [JsonProperty("conference_id")]
        public string ConferenceId { get; set; }

        [JsonProperty("start_date")]
        public string StartDate { get; set; }

        [JsonProperty("duration")]
        public float Duration { get; set; }

        [JsonProperty("participants")]
        public int Participants { get; set; }

        [JsonProperty("ongoing")]
        public bool IsOngoing { get; set; }
    }
}
