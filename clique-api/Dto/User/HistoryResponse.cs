﻿namespace CliqueApi.Dto.User
{
    using System.Collections.Generic;

    using Newtonsoft.Json;

    public class HistoryResponse : BaseResponse
    {
        [JsonProperty("history")]
        public List<History> Histories { get; set; }
    }
}
