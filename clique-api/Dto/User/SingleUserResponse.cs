﻿namespace CliqueApi.Dto.User
{
    using Newtonsoft.Json;

    public class SingleUserResponse : BaseResponse
    {
        [JsonProperty("user")]
        public User User { get; set; }
    }
}