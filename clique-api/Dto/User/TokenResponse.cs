﻿namespace CliqueApi.Dto.User
{
    using Newtonsoft.Json;

    public class TokenResponse : BaseResponse
    {
        [JsonProperty("token")]
        public string Token { get; set; }
    }
}
