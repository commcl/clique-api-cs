﻿namespace CliqueApi.Dto.User
{
    using System.Collections.Generic;

    using Newtonsoft.Json;

    public class UsersResponse : BaseResponse
    {
        [JsonProperty("result")]
        public List<User> Users { get; set; }
    }
}
