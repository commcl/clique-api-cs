namespace CliqueApi
{
    using System.Threading.Tasks;

    using CliqueApi.Dto;
    using CliqueApi.Dto.Common;
    using CliqueApi.Dto.Conference;
    using CliqueApi.Dto.Customer;
    using CliqueApi.Dto.Invite;
    using CliqueApi.Dto.Nodes;
    using CliqueApi.Dto.User;

    using RestEase;

    /// <summary>
    /// Interface of Clique API .NET wrapper.
    /// </summary>
    [Header("User-Agent", "CSharp wrapper")]
    public interface ICliqueApi
    {
        /// <summary>
        /// Header authorization key.
        /// </summary>
        [Header("Authorization")]
        string ApiKey { get; set; }

        #region - Conference -

        /// <summary>
        /// Assign fs domain for conference.
        /// </summary>
        /// <param name="id"> The conference unique ID. </param>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        [Post("conferences/{id}/start")]
        Task<SingleConferenceResponse> ConferenceStart([Path("id")] string id);

        /// <summary>
        /// Create new conference.
        /// </summary>
        /// <param name="conference"> Creation data. </param>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        [Post("conferences")]
        Task<SingleConferenceResponse> ConferenceCreate([Body] Conference conference);

        /// <summary>
        /// Get conference settings.
        /// </summary>
        /// <param name="id"> The conference unique ID. </param>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        [Get("conferences/{id}/settings")]
        Task<SettingsResponse> ConferenceGetSettings([Path("id")] string id);

        /// <summary>
        /// Get user list for conference.
        /// </summary>
        /// <param name="id"> The conference unique ID. </param>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        [Get("conferences/{id}/user-list-hash")]
        Task<ConferenceUserListResponse> ConferenceUserList([Path("id")] string id);

        /// <summary>
        /// Get the conference by unique ID.
        /// </summary>
        /// <param name="id"> The conference unique ID. </param>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        [Get("conferences/{id}")]
        Task<SingleConferenceResponse> ConferenceGetById([Path("id")] string id);

        /// <summary>
        /// Search users by params in model <see cref="Conference"/>.
        /// </summary>
        /// <param name="conference"> Model for search. </param>
        /// <returns>
        /// The async <see cref="Task"/> with response.
        /// </returns>
        [Get("conferences")]
        [SerializationMethods(Query = QuerySerializationMethod.Serialized)]
        Task<ConferencesResponse> ConferencesSearch([Query]Conference conference);

        /// <summary>
        /// Update conference info.
        /// </summary>
        /// <param name="id"> The conference unique ID. </param>
        /// <param name="conference"> Updating conference. </param>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        [Put("conferences/{id}")]
        Task<SingleConferenceResponse> ConferenceUpdate([Path("id")] string id, [Body]Conference conference);

        /// <summary>
        /// Update conference settings.
        /// </summary>
        /// <param name="id"> The conference unique ID. </param>
        /// <param name="settings"> Conference settings object. </param>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        [Put("conferences/{id}/settings")]
        Task<SettingsResponse> ConferenceUpdateSettings([Path("id")] string id, [Body] Settings settings);

        #endregion // Conference

        #region - Customer -

        /// <summary>
        /// Create new customer.
        /// </summary>
        /// <param name="query"> Object fo customer creation <see cref="CustomerCreationQuery"/>. </param>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        [Post("customers")]
        Task<SingleCustomerResponse> CustomerCreate([Body] CustomerCreationQuery query);

        /// <summary>
        /// Get customer by unique ID.
        /// </summary>
        /// <param name="id"> The customer unique ID. </param>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        [Get("customers/{id}")]
        Task<SingleCustomerResponse> CustomerGetById([Path("id")] int id);

        /// <summary>
        /// Search customers by params in model <see cref="Customer"/>.
        /// </summary>
        /// <param name="customer">. Model for search. </param>
        /// <returns>
        /// The async <see cref="Task"/> with response.
        /// </returns>
        [Get("customers")]
        [SerializationMethods(Query = QuerySerializationMethod.Serialized)]
        Task<CustomersResponse> CustomerSearch([Query]Customer customer);

        /// <summary>
        /// Update customer info.
        /// </summary>
        /// <param name="id"> The customer unique ID. </param>
        /// <param name="customer"> Customer info for updating. </param>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        [Put("customers/{id}")]
        Task<SingleCustomerResponse> CustomerUpdate([Path("id")] int id, [Body] Customer customer);

        #endregion // Customer

        #region - Invitation -

        /// <summary>
        /// Create new invitation template.
        /// </summary>
        /// <param name="template"> Template object. </param>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        [Post("invites/templates")]
        Task<TemplateResponse> InvitationTemplateCreate([Body]Template template);

        /// <summary>
        /// Get invitation template by name.
        /// </summary>
        /// <param name="name"> Template unique name. </param>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        [Get("invites/templates/{name}")]
        Task<TemplateResponse> InvitationTemplateGetById([Path("name")]string name);

        /// <summary>
        /// Update invitation template.
        /// </summary>
        /// <param name="name"> Template unique name. </param>
        /// <param name="template"> Template body, use {param} syntax for template parameters. </param>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        [Put("invites/templates/{name}")]
        Task<TemplateResponse> InvitationTemplateUpdate([Path("name")]string name, [Body]Template template);

        /// <summary>
        /// The delete invitation template.
        /// </summary>
        /// <param name="name"> Template unique name. </param>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        [Delete("invites/templates/{name}")]
        Task<BaseResponse> InvitationTemplateDelete([Path("name")]string name);

        /// <summary>s
        /// The get initial data.
        /// </summary>
        /// <param name="uuid"> The user unique ID. </param>
        /// <param name="conferenceId"> The conference id. </param>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        [Get("invites/initial-data")]
        Task<InvitesResponse> GetInitialData([Query("user_id")] string uuid, [Query("conf_id")] string conferenceId);

        /// <summary>
        /// Send invitations for target conference.
        /// </summary>
        /// <param name="conferenceId"> The conference id. </param>
        /// <param name="msg"> The msg. </param>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        [Post("invites/{cid}/send")]
        Task<SendingInviteResponse> SendInvitesForConference([Path("cid")] string conferenceId, [Body]InviteMessages msg);

        #endregion // Invitation

        #region - Nodes -

        /// <summary>
        /// The get active nodes.
        /// </summary>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        [Get("nodes")]
        Task<NodesResponse> NodesGetActive();

        #endregion // Nodes

        #region - User -

        /// <summary>
        /// Create new user.
        /// </summary>
        /// <param name="user"> User data. </param>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        [Post("users")]
        Task<SingleUserResponse> UserCreate([Body] User user);

        /// <summary>
        /// Get user by unique ID.
        /// </summary>
        /// <param name="uuid"> The user unique ID. </param>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        [Get("users/{uuid}")]
        Task<SingleUserResponse> UserGetByUuid([Path("uuid")] string uuid);

        /// <summary>
        /// Recreate and return user access token.
        /// </summary>
        /// <param name="uuid"> The user unique ID. </param>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        [Get("token/{uuid}")]
        Task<TokenResponse> UserGetToken([Path("uuid")] string uuid);

        /// <summary>
        /// Get history for user.
        /// </summary>
        /// <param name="uuid"> The user unique ID. </param>
        /// <param name="top"> History limit (max 20). </param>
        /// <param name="skip"> History offset. </param>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        [Get("users/{uuid}/call_history")]
        Task<HistoryResponse> UserGetHistory([Path("uuid")] string uuid, [Query("top")] int top, [Query("skip")] int skip);

        /// <summary>
        /// Search users by params in model <see cref="User"/>.
        /// </summary>
        /// <param name="user"> Model for search. </param>
        /// <returns>
        /// The async <see cref="Task"/> with response.
        /// </returns>
        [Get("users")]
        [SerializationMethods(Query = QuerySerializationMethod.Serialized)]
        Task<UsersResponse> UsersSearch([Query]User user);

        /// <summary>
        /// Update user info.
        /// </summary>
        /// <param name="uuid"> The user unique ID. </param>
        /// <param name="user"> User model for updating. </param>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        [Put("users/{uuid}")]
        Task<SingleUserResponse> UserUpdate([Path("uuid")] string uuid, [Body] User user);

        #endregion // User
    }
}