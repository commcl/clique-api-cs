﻿namespace CliqueApi
{
    using System;
    using System.Collections.Generic;
    using System.Linq;

    using Newtonsoft.Json;

    using RestEase;

    /// <summary>
    /// Serialization class for query of GET requests.
    /// </summary>
    internal class SearchQuerySerializer : IRequestQueryParamSerializer
    {
        /// <summary>
        /// Serialize a query parameter whose value is scalar (not a collection), into a collection of name -&gt; value pairs
        /// </summary>
        /// <remarks>
        /// Most of the time, you will only return a single KeyValuePair from this method. However, you are given the flexibility,
        /// to return multiple KeyValuePairs if you wish. Duplicate keys are allowed: they will be serialized as separate query parameters.
        /// </remarks>
        /// <typeparam name="T">Type of the value to serialize</typeparam>
        /// <param name="name">Name of the query parameter</param>
        /// <param name="value">Value of the query parameter</param>
        /// <param name="info">Extra info which may be useful to the serializer</param>
        /// <returns>A colletion of name -&gt; value pairs to use as query parameters</returns>
        public IEnumerable<KeyValuePair<string, string>> SerializeQueryParam<T>(string name, T value, RequestQueryParamSerializerInfo info)
        {
            var objectType = typeof(T);

            foreach (var property in objectType.GetProperties())
            {
                var attr = property.GetCustomAttributes(typeof(JsonPropertyAttribute), false).FirstOrDefault();
                if (attr != null)
                {
                    var propertyValue = property.GetValue(value);
                    var propertyDefaultValue = GetDefaultValue(property.PropertyType);
                    if (propertyValue != null && !propertyValue.Equals(propertyDefaultValue))
                    {
                        var jsonAttr = (JsonPropertyAttribute)attr;

                        yield return new KeyValuePair<string, string>(jsonAttr.PropertyName, propertyValue.ToString());
                    }
                }
            }
        }

        /// <summary>
        /// Serialize a query parameter whose value is a collection, into a collection of name -&gt; value pairs
        /// </summary>
        /// <remarks>
        /// Most of the time, you will return a single KeyValuePair for each value in the collection, and all will have
        /// the same key. However this is not required: you can return whatever you want.
        /// </remarks>
        /// <typeparam name="T">Type of the value to serialize</typeparam>
        /// <param name="name">Name of the query parameter</param>
        /// <param name="values">Values of the query parmaeter</param>
        /// <param name="info">Extra info which may be useful to the serializer</param>
        /// <returns>A colletion of name -&gt; value pairs to use as query parameters</returns>
        public IEnumerable<KeyValuePair<string, string>> SerializeQueryCollectionParam<T>(string name, IEnumerable<T> values, RequestQueryParamSerializerInfo info)
        {
            yield break;
        }

        /// <summary>
        /// Return defaut value for type.
        /// </summary>
        /// <param name="t">Input type.</param>
        /// <returns>Default value for type.</returns>
        private static object GetDefaultValue(Type t)
        {
            return t.IsValueType ? Activator.CreateInstance(t) : null;
        }
    }
}
