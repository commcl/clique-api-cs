Solution structure:
	- clique-api - wrapper for REST API cluque,
	- clique-api-test - unit tests for wrapper.

For using wrapper you need reference assembly RestEase v1.4.1 from NuGet packages.

Usage:

	1. Firstly you need creation API interface object by helper who located in clicue-api/ApiHelper.cs (recomended) or manually.
For use you must specify the base web-address of REST API and customer authorization key.

	2. For use API, you must call the methods of ICliqueApi interface in interface object context. Examples you can see in unit test project cluque-api-test.